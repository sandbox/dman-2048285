Theme an HTML element
=====================

This is just a slightly more useful version of theme_html_tag() from core.
It actually recurses and renders child elements if you set up a render array
that uses this as a #type.

Core #type='html_tag' is a deadbeat dad : does not support its children.

Motivation
----------

The Drupal Form API and render array are almost feature complete, but it still
cannot be used as a full page builder.

Using '#type'=>'container' in FAPI will always make the container a div.

Using '#theme'=>'html_tag' will sometimes render singletons (eg for iframe)
which can break layout.
A workaround for this can be to set '#value'=>''
@see theme_html_tag() which is pretty flaky.

Using #type or #theme html_tag will allow you to use arbitrary tags types,
but will NOT recurse into element_children() as you would expect.

Neither seem to support setting the @id automatically either, though it
can be shoehorned in under #attributes.

This element type is the missing link that makes FAPI and render work as 
designed.

Proof-of-concept
----------------

You should not HAVE to use this element as a dependency, as usually you can
work around this issue by slightly changing what you want from markup,
or by abusing #prefix and #suffix.
Thos module is only here to point out that #type=html_tag is currently 
functionally incomplete and #type=container isn't as flexible as it should be.


Usage
-----

  $form['preview-container'] = array(
    '#type' => 'html_element',
    '#tag' => 'section'
    'preview-frame' => array(
      '#type' => 'html_element',
      '#tag' => 'iframe',
      '#value'=>'', // Set this to avoid singletons.
      '#attributes' => array(
        'src' => $preview_uri,
        'id' => 'edit-preview-frame',
      ),
    ),
  );
  

If you use 
  '#type' => 'html_element',
without defining #tag, 'div' will be assumed.

